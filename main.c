#include "header.h"
#include<stdlib.h>
#include<stdio.h>
#include<string.h>

// PROGRAM 1

void load(float *arrX, float *arrY, float *arrZ, int length);
void print(float *arrX, float *arrY, float *arrZ, int length);

int main(){

    int size = 50;
    float *arrX=calloc(size,sizeof(float));
    float *arrY=calloc(size,sizeof(float));
    float *arrZ=calloc(size,sizeof(float));
    load(arrX,arrY,arrZ,size);
    print(arrX,arrY,arrZ,size);

    float x_mean = mean(arrX,size);
    float y_mean = mean(arrY,size);
    float z_mean = mean(arrZ,size);

    float x_median = median(arrX,size);
    float y_median = median(arrY,size);
    float z_median = median(arrZ,size);
    
    float x_std = std(arrX,x_mean,size);
    float y_std = std(arrY,y_mean,size);
    float z_std = std(arrZ,z_mean,size);

    printf("\n\nMean: %f %f %f\n", x_mean,y_mean,z_mean);
    printf("Median: %f %f %f\n", x_median,y_median,z_median);
    printf("Standard Deviation: %f %f %f\n", x_std,y_std,z_std);

    FILE *file;
    file = fopen("P0001_attr.rec.txt", "ab");
    fprintf(file, "\nAvg: %f %f %f\nMedian: %f %f %f\nSTD dev.: %f %f %f",
        x_mean,y_mean,z_mean,
        x_median,y_median,z_median,
        x_std,y_std,z_std);

    return 0;
}

void load(float *arrX, float *arrY, float *arrZ, int length){
    FILE *file;
    file = fopen("P0001_attr.rec.txt", "r");
    int arr_main[50];

    if (file == NULL){
        printf("Null pointer - file not found");
        exit(1);
    }

    fseek(file, 13, SEEK_SET);
    for (int i = 0; i < length; i++){
        fscanf(file, "%d.\t%f \t%f\t%f\n", 
            &arr_main[i], &arrX[i], &arrY[i], &arrZ[i]);
    }
    fclose(file);
    return;
}

void print(float *arrX, float *arrY, float *arrZ, int length){
    printf("LP\tX\t\tY\t\tRHO\n\n");
    for (int i = 0; i < length; i++){
        printf("%d\t%.3f\t\t%.5f\t%.3f\n",
            i+1,arrX[i],arrY[i],arrZ[i]);
    }
    return;
}