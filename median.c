#include "header.h"
#include<math.h>

float median(float arr[],int length)
{
    float median = 0.0;
    if (length % 2 == 0)
    {
        median = (arr[(length-1)/2] + arr[length/2])/2;
    }
    else
    {
        median = arr[length/2];
    }

    return median;
}
