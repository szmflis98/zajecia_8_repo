CC=gcc
CLAGS=-Wall
LDLIBS=-lm

main: main.o mean.o std.o median.o
	$(CC) $(CFLAGS) -o main main.o mean.o std.o median.o  $(LDLIBS)

main.o: main.c header.h
	$(CC) $(CFLAGS) -c main.c

mean.o: mean.c
	$(CC) $(CFLAGS) -c mean.c

std.o: std.c
	$(CC) $(CFLAGS) -c std.c

median.o: median.c 
	$(CC) $(CFLAGS) -c median.c 
