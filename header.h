#ifndef header_h
#define header_h

float mean(float *arr, int length);
float std(float *arr, float avg, int length);
float median(float *arr, int length);

#endif
