#include "header.h"
#include <math.h>

float std(float arr[], float avg, int length)
{
    double temp = 0.0;
    int i = 0;
    for (i = 0; i<length; i++)
    {
        temp += pow(arr[i]-avg,2);
    }

    double std_dev = sqrt(temp/50);

    return (double) std_dev;
}
